using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnWay : MonoBehaviour
{
    public GameObject Player;
    public GameObject[] ways;
    public GameObject[] sticks;
    public GameObject tempStick;
    public GameObject stone;
    public GameObject tree;
    public GameObject stick;
    public GameObject temp;
    Player player;

    public float helperStick = 0;

    //private float[] positions = { -0.47f, 0.075f, 0.60f };

    public bool tempWay;
    //public bool single;

    private float tempX;

    public GameObject[] allWays;
    private int index;
    private int indexOption;
    //public Grass

    private void Start()
    {
        Player = GameObject.Find("chicken");
        player = Player.GetComponent<Player>();
        tempX = Player.transform.position.x -12f;
    }
    void Update()
    {

/*        if (helperStick == 1)
        {
            stick.transform.localPosition += -transform.right * 2f * Time.deltaTime;
        }*/

        MoveStick();

        tempWay = player.spawn;
        SpawnRoadOrGrass();
        player.spawn = false;
    }


    void MoveStick()
    {
        stick.transform.localPosition += -transform.right * 2f * Time.deltaTime;
    }
    private void SpawnRoadOrGrass()
    {
        if (tempWay == true)
        {     
/*            for (int i = 1; i < 4; i++)
            {*/
            tempX -= 1f;
            index = Random.Range(0, ways.Length);
                Instantiate(
                    ways[index],
                    new Vector3(tempX, 0f, 0f),
                    Quaternion.Euler(new Vector3(0, 0, 0))
                    );
            if (index == 0)
            {
                for (int i = 0; i < Random.Range(1,3); i++)
                {
                    indexOption = Random.Range(0, 2);

                    if (indexOption == 0)
                    {
                        temp = tree;
                    }
                    if (indexOption == 1)
                    {
                        temp = stone;
                    }

                    Instantiate(
                        temp,
                        new Vector3(tempX, 0f, Random.Range(-12, 12)),
                        Quaternion.Euler(new Vector3(0, 0, 0))
                        );

                }
            }

            if (index == 2)
            {
                for(int i = 0; i < Random.Range(1,6); i++)
                {

                    Instantiate(
                        stick,
                        new Vector3(tempX, 0f, 12f),
                        Quaternion.Euler(new Vector3(0, 0, 0))
                        );
                }

                
            }

            tempWay = false;
/*            }*/     
        }
    }

}
