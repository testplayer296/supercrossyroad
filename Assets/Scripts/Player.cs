using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public GameObject SpawnWay;
    public GameObject Camera;

    SpawnWay spawnWay;

    [SerializeField] KeyCode keyW;
    [SerializeField] KeyCode keyS;
    [SerializeField] KeyCode keyA;
    [SerializeField] KeyCode keyD;
    [SerializeField] float speed;

    public float stickHelper = 0;

    public Rigidbody rb;
    public Rigidbody rbCamera;

    public bool ground;
    public bool spawn;

    public bool tempSingle;

    public GameObject Stick;

    private void Start()
    {
        SpawnWay = GameObject.Find("chicken");
        spawnWay = SpawnWay.GetComponent<SpawnWay>();
    }
    void Update()
    {
        if (stickHelper == 1)
        {
            transform.localPosition += -transform.right * 1f * Time.deltaTime;
        }
        
        //tempSingle = spawnWay.single;
        GetInput();
        //CameraPosition();
        //Camera.transform.rotation = Quaternion.Euler(45f, -120f, 0f);
        //Camera.transform.localPosition += transform.forward * 1f * Time.deltaTime;
        rbCamera.velocity = new Vector3(-1f, 0f, 0f);
    }

    public bool SpawnTest()
    {
        if (spawn == true)
            return true;
        else
            return false;
    }
    private void CameraPosition()
    {
        Camera.transform.position = new Vector3(transform.position.x + 2.32f, transform.position.y + 5f, transform.position.z + 2.37f);
        //rbCamera.velocity = new Vector3(transform.position.x + 2.32f, transform.position.y + 5f, transform.position.z + 2.37f);
    }

    private void GetInput()
    {
        if (Input.GetKeyDown(keyW))
        {
            if (ground == true)
            {
                transform.rotation = Quaternion.Euler(transform.rotation.x, -90f, transform.position.y);
                rb.velocity = new Vector3(-2.57f, 2f, 0);
                spawn = true;
                //CameraPosition();
            }
    }
        if (Input.GetKeyDown(keyS))
        {
            if (ground == true)
            {
                transform.rotation = Quaternion.Euler(transform.rotation.x, 90f, transform.position.y);
                rb.velocity = new Vector3(2.57f, 2f, 0);
                //CameraPosition();
            }

        }
        if (Input.GetKeyDown(keyA))
        {
            if (ground == true)
            {
                transform.rotation = Quaternion.Euler(transform.rotation.x, -180f, transform.position.y);
                rb.velocity = new Vector3(0f, 2f, -2.57f);
                //CameraPosition();

            }

        }
        if (Input.GetKeyDown(keyD))
        {
            if (ground == true)
            {
                transform.rotation = Quaternion.Euler(transform.rotation.x, 0f, transform.position.y);
                rb.velocity = new Vector3(0f, 2f, 2.57f);
                //CameraPosition();
            }
        }
        
        if (Input.GetKeyUp(keyW))
        {
            spawn = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            ground = true;
        }

        if (collision.gameObject.tag == "Water")
        {
            Destroy(gameObject);
        }

        if (collision.gameObject.tag == "stick")
        {
            stickHelper = 1;
            ground = true;
            //transform.localPosition += -transform.forward * 1f * Time.deltaTime;
            //transform.SetParent(Stick.transform);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            ground = false;
        }

        if (collision.gameObject.tag == "stick")
        {
            stickHelper = 0;
            ground = true;
            //transform.localPosition += -transform.forward * 1f * Time.deltaTime;
            //transform.SetParent(Stick.transform);
        }
    }

}
