using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSticks : MonoBehaviour
{
    public Transform Player;
    //public GameObject Stick;
    void Start()
    {
        Player = GameObject.Find("Player").transform;
    }

    void Update()
    {
        transform.localPosition += -transform.forward * 1f * Time.deltaTime;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            transform.localPosition += -transform.forward * 1f * Time.deltaTime;
            //Player.SetParent(gameObject.transform);
        }
    }
}
